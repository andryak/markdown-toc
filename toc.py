from collections import namedtuple
from mistune import Renderer, Markdown


Node = namedtuple('Node', ['value', 'parent', 'children'])
Header = namedtuple('Header', ['id', 'text', 'level'])


def print_nodes(nodes):
    if nodes:
        print('<ul>')
        for node in nodes:
            print('<li>')
            print(f"<a href='#toc-{node.value.id}'>{node.value.text}</a>")
            print_nodes(node.children)
            print('</li>')
        print('</ul>')


class TocRenderer(Renderer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.headers = []
    
    def header(self, text, level, raw=None):
        """This is called whenever the markdown parser using this renderer finds a header."""
        header = Header(id=len(self.headers), text=text, level=level)
        self.headers.append(header)
        return f"<h{level} id='toc-{header.id}'>{text}</h{level}>"

    def clear(self):
        self.headers.clear()

    def headers_to_tree(self):
        fake_root_header = Header(None, None, -1)
        current_node = Node(value=fake_root_header, parent=None, children=[])
        for header in self.headers:
            while header.level <= current_node.value.level:
                current_node = current_node.parent

            node = Node(value=header, parent=current_node, children=[])
            current_node.children.append(node)
            current_node = node
        
        while current_node.parent is not None:
            current_node = current_node.parent
        return current_node

    def print_toc(self, toc_title='Table of Contents'):
        print(f'<h1>{toc_title}</h1>')
        tree = self.headers_to_tree()
        print_nodes(tree.children)


if __name__ == '__main__':
    import sys

    filename = sys.argv[1]
    with open(filename, 'r') as fh:
        content = fh.read()
        
        renderer = TocRenderer()
        md = Markdown(renderer=renderer)
        html = md.parse(content)
        renderer.print_toc()
        print(html)
